import React,{Component} from "react";
import {connect} from "react-redux";
import actions from "../app/actions.js";
import BookForm from "../component/BookForm.js";
import BooksList from "../component/BooksList.js";
import Search from "../component/Search.js";
import Sort from "../component/Sort.js";

class AppView extends Component {

    render() {
        return (
            <div className="home__wrapper">
                <div className="home">
                    <div className="home__bookform">
                        <BookForm addBook={this.props.addBook} {...this.props}/>
                    </div>
                    <div className="home__contents">
                        <div className="home__filter">
                            <Sort sortBook={this.props.sortBook} {...this.props} />
                        </div>
                        <div className="home__search">
                            <Search searchBook={this.props.searchBook} {...this.props}/>
                        </div>
                        <div className="home__content">
                            <BooksList {...this.props} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
;

function mapStateToProps(state) {
    return {
        books: state.get("books"),
        edit: state.get("edit"),
        search: state.get("search"),
        sort: state.get("sort")
    };
}

module.exports = connect(mapStateToProps, actions)(AppView);