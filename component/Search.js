import React,{Component} from "react";

class Search extends Component {
    constructor(props) {
        super(props);
    }

    onKeyUp(){
       let text=this.refs.search.value;
        this.props.searchBook(text);
    }

    render() {
        return (

            <fieldset className="search">
                <legend>Поиск</legend>
                <div className="inputBlock">
                    <input
                        onKeyUp={this.onKeyUp.bind(this)}
                        ref="search"
                        name="search"
                        type="text"
                        placeholder="Введите название книги"
                        className="inputBlock__input"
                        />
                </div>
            </fieldset>

        )

    }
};

module.exports = Search;