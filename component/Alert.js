import React,{Component} from "react";

class Sort extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (

            <div className="Alert__wrapper" style={{display:"none"}}>
                <div className="Alert">
                    <label className="Alert__title">Хотите продолжить удаление?</label>
                    <div className="Alert__btn" >
                        <button
                            onClick={this.props.onClick}
                            value="true"
                            className=""
                        >ДА
                        </button>
                    </div>
                    <div className="Alert__btn">
                        <button

                            onClick={this.props.onClick}

                            value="false"
                            className=""
                        >НЕТ
                        </button>
                    </div>
                </div>
            </div>

        )

    }
};

module.exports = Sort;