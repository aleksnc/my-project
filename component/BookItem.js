import React,{Component} from "react";
import Alert from "./Alert.js";

class BookItem extends Component {
    constructor(props) {
        super(props);
        this.deleteBtn = this.deleteBtn.bind(this);
        this.editBtn = this.editBtn.bind(this);
        this.toggle = this.toggle.bind(this);
        this.alertFunk = this.alertFunk.bind(this);
        this.answerAlert = this.answerAlert.bind(this);
    }

    toggle(e) {

      let  el = this.alertFunk(e.target);
        console.log(el);

        el.style.display = (el.style.display == 'none') ? '' : 'none'
    }


    answerAlert(e) {
        let text = e.target.value;

        let  el = e.target.parentNode.parentNode.parentNode;

        el.style.display = (el.style.display == 'none') ? '' : 'none'


        if(text!="true"){
           return;
       }
        this.deleteBtn();
    }

    alertFunk(e) {
      let  parent = e.parentNode.parentNode;
        return parent.lastChild;
    }

    deleteBtn() {
        this.props.deleteBook(this.props.book);
        let arrBook = JSON.parse(localStorage.getItem("book"));
        let newArr = [];
        arrBook.map((item, indx) => {
                if (indx != this.props.dataId) {
                    newArr.push(item)
                }
            }
        )


        localStorage.removeItem("book");
        localStorage.setItem("book", JSON.stringify(newArr));

    }

    editBtn() {
        Object(this.props.book)['dataId'] = this.props.dataId;
        this.props.editBook(this.props.book)
    }

    render() {

        return (



            <div className="BookItem">
                <div className="BookItem__name">Название: {this.props.book.name}</div>
                <div className="BookItem__autor">Автор: {this.props.book.autor}</div>
               {/* <div className="BookItem__autor">Стр: {this.props.book.pages}</div>*/}
                <div className="BookItem__btn">
                    <button data-item={this.props.dataId} onClick={this.toggle}>Удалить</button>
                </div>
                <div className="BookItem__btn BookItem__btn--edit">
                    <button data-item={this.props.dataId} onClick={this.editBtn}>Редактировать</button>
                </div>


                <Alert onClick={this.answerAlert}/>
            </div>

        )

    }
};
module.exports = BookItem;