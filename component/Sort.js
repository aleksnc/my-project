import React,{Component} from "react";

class Sort extends Component {
    constructor(props) {
        super(props);
    }

    onChange(e) {
        let text = e.target.value;
        this.props.sortBook(text);
    }

    render() {
        return (

            <fieldset className="sort" onChange={this.onChange.bind(this)}>
                <legend>Сортировка</legend>
                <div className="inputDot">
                    <input
                        ref="sortName"
                        value="name"
                        name="sort"
                        type="radio"
                        className="inputDot__input"
                        id="sortName"
                    />
                    <label htmlFor="sortName" className="inputDot__label">
                        <span className="inputDot__dot inputDot__dot_radio"> </span>
                        <span className="inputDot__text">По названию</span>
                    </label>
                </div>
                <div className="inputDot">
                    <input
                        ref="sortAutor"
                        value="autor"
                        name="sort"
                        type="radio"
                        id="sortAutor"
                        className="inputDot__input"
                    />
                    <label htmlFor="sortAutor" className="inputDot__label">
                        <span className="inputDot__dot inputDot__dot_radio"> </span>
                        <span className="inputDot__text">По автору</span>
                    </label>
                </div>

            </fieldset>

        )

    }
};

module.exports = Sort;