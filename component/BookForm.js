import React,{Component} from "react";
import actions from "../app/actions.js";

class BookForm extends Component {
    constructor(props) {
        super(props);
    }

    onClick() {
        let arrBook = [];

        let storageBook = JSON.parse(localStorage.getItem("book"));
        if (this.refs.name.value !== "" && this.refs.autor.value !== "" && this.refs.year.value !== "" && this.refs.pages.value !== "") {
            var contentBook = {
                name: this.refs.name.value,
                autor: this.refs.autor.value,
                year: this.refs.year.value,
                pages: this.refs.pages.value
            }

            if (storageBook != null) {
                arrBook = JSON.parse(localStorage.getItem("book"));
            }

            if(Object(this.props.edit)["name"]!==undefined) {
                arrBook[this.props.edit.dataId] = contentBook;
                this.props.deleteBook(this.props.edit);
            } else {
                arrBook.push(contentBook);
            }

            localStorage.removeItem("book");
            localStorage.setItem("book", JSON.stringify(arrBook));

            this.refs.name.value = "";
            this.refs.autor.value = "";
            this.refs.year.value = "";
            this.refs.pages.value = "";

            if(Object(this.props.edit)["name"]!==undefined) {
                for (let i in Object(this.props.edit)) {
                    delete Object(this.props.edit)[i];
                }
            }

            return this.props.addBook(contentBook);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.edit.name) {
            return;
        }

        for (let i in this.refs) {
            this.refs[i].value = nextProps.edit[i];
        }
    }

    render() {

        return (
            <div className="formBook">
                <fieldset className="formBook__inputs">
                    <legend>Настройки</legend>
                    <div className="inputBlock">
                        <label className="inputBlock__title">Название</label>
                        <input
                            ref="name"
                            name="name"
                            type="text"
                            className="inputBlock__input"
                        />
                    </div>
                    <div className="inputBlock">
                        <label className="inputBlock__title">Автор</label>
                        <input
                            ref="autor"
                            name="autor"
                            type="text"
                            className="inputBlock__input"
                        />
                    </div>
                    <div className="inputBlock">
                        <label className="inputBlock__title">Год</label>
                        <input
                            ref="year"
                            name="year"
                            type="month"
                            className="inputBlock__input"
                        />
                    </div>
                    <div className="inputBlock">
                        <label className="inputBlock__title">Кол-во страниц</label>
                        <input
                            ref="pages"
                            name="pages"
                            type="number"
                            className="inputBlock__input"
                        />
                    </div>

                    <div className="formBook__button">
                        <button onClick={this.onClick.bind(this)}>
                            {
                                this.props.edit.dataId !== undefined &&
                                <span>Сохранить</span>
                            }
                            {
                                this.props.edit.dataId === undefined &&
                                <span>Добавить</span>
                            }
                        </button>
                    </div>
                </fieldset>
            </div>
        )
    }
};

module.exports = BookForm;