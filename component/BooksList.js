import React,{Component} from "react";
import BookItem from "./BookItem.js";

class BooksList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let arrFilter=[];
        let search = this.props.search;
        let sortKey = this.props.sort;

        let book = false;
        if (this.props.books.size === 0) {
          //  console.log(JSON.parse(localStorage.getItem("book")));

            if (JSON.parse(localStorage.getItem("book")) != null) {
               let arrBook = JSON.parse(localStorage.getItem("book"));

              //  console.log('arrBook',arrBook);

                arrBook.map((item,indx)=>{
                  //  console.log(item);

                    this.props.addBook(item);

                    arrFilter.push(item);
                })
            }
        } else{
            arrFilter = this.props.books;
        }

        book = arrFilter.filter(
            function (value) {
                return (value['name'].indexOf(search)!=-1);
            }
        );

        book = book.sort(function (a, b) {

            if (a[sortKey] > b[sortKey]) {
                return 1;
            }
            if (a[sortKey] > b[sortKey]) {
                return -1;
            }
            // a должно быть равным b
            return 0;
        });


        return (

                <fieldset className="bookList">
                    <legend>Список книг</legend>
                    {
                        book &&
                        book.map((item, indx) =>
                            <div className="bookList__book" key={indx}>
                            <BookItem
                                dataId={indx}
                                book={item}
                                deleteBook={this.props.deleteBook}
                                editBook={this.props.editBook}/>
                            </div>
                        )
                    }
                </fieldset>
        )
    }
};
module.exports = BooksList;