import React from "react";
import ReactDOM from "react-dom";
import Redux,{createStore} from "redux";
import {Provider} from "react-redux";
import reducer from "./reducer.js";
import AppView from "../route/index.js";


var store = createStore(reducer);

store.dispatch({
    type: "SET_STATE",
    state: {
        books: [],
        edit: [],
        search: "",
        sort: "",
    }
});


ReactDOM.render(
    <Provider store={store}>
        <AppView/>
    </Provider>,
    document.getElementById("container")
);