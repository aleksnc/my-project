var addBook = function (book) {
    return {
        type: "ADD_BOOK",
        book
    }
};
var deleteBook = function (book) {
    return {
        type: "DELETE_BOOK",
        book
    }
};

var editBook = function (book) {
    return {
        type: "EDIT_BOOK",
        book,
    }
};


var searchBook = function (name) {
    return {
        type: "SEARCH_BOOK",
        name,
    }
};

var sortBook = function (sorts) {
    return {
        type: "SORT_BOOK",
        sorts,
    }
};



module.exports = {addBook, deleteBook, editBook, searchBook, sortBook};