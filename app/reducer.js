import {Map} from "immutable";

var reducer = function(state = Map(), action) {
    switch (action.type) {
        case "SET_STATE":
            return state.merge(action.state);
        case "ADD_BOOK":
            return state.update("books", (books) => books.push(action.book));
        case "DELETE_BOOK":
            return state.update("books",
                (books) => books.filterNot(
                    (item) => item === action.book

                )
            );
        case "EDIT_BOOK":
            return(
                state.update("edit", (edit) => edit=action.book)
            );
        case "SEARCH_BOOK":
            return(
                state.update("search", (search) => search=action.name)
            );
        case "SORT_BOOK":
            return(
                state.update("sort", (sorts) => sorts=action.sorts)
            );
    }
    return state;
}
module.exports = reducer;